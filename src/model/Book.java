package model;

import java.sql.Date;
import java.util.Objects;

public class Book  implements Comparable<Book>{
	private int bookid;
	private String title;
	private String author;
	private double price;
	private Date publishDt;

	public Book() {

	}

	public Book(int bookid, String title, String author, double price, Date publishDt){
		super();
		this.bookid = bookid;
		this.title = title;
		this.author = author;
		this.price = price;
		this.publishDt = publishDt;
	}

	public int getBookid() {
		return bookid;
	}
	public void setBookid(int bookid) {
		this.bookid = bookid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getPublishDt() {
		return publishDt;
	}
	public void setPublishDt(Date publishDt) {
		this.publishDt = publishDt;
	}

	@Override
	public int hashCode() {
		return Objects.hash(author, bookid, price, publishDt, title);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		return Objects.equals(author, other.author) && bookid == other.bookid
				&& Double.doubleToLongBits(price) == Double.doubleToLongBits(other.price)
				&& Objects.equals(publishDt, other.publishDt) && Objects.equals(title, other.title);
	}

	@Override
	public String toString() {
		return "Book [bookid=" + bookid + ", title=" + title + ", author=" + author + ", price=" + price
				+ ", publishDt=" + publishDt + "]";
	}

	@Override
	public int compareTo(Book o) {
		if(bookid>o.bookid) {
			return 1;
		}
		else if(bookid<o.bookid){
			return -1;
		}
		return 0;
	}

}