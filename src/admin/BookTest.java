package admin;


import java.sql.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.TreeSet;

import database.BookData;
import exceptions.RecordEmptyException;
import model.Book;


public class BookTest {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner in=new Scanner(System.in);
		int c;
		do {
			System.out.println("0.Exit\n1.View Books\n2.Search Books\n3.Add Books\n4.Delete Books\n5.Update Books");
			System.out.println();
			System.out.print("Enter Choice :");
			c=in.nextInt();
			switch(c) {
			case 1: 
				System.out.println("Viewing book");
				try {
					TreeSet<Book> bookList=new BookData().viewBook();
					for(Book book:bookList) {
						System.out.println(bookList);
						System.out.println(book.getBookid()+" : "+book.getTitle());
					}
				} catch (RecordEmptyException e) {
					System.out.println(e.getMessage());
				}
				System.out.println();
				break;

			case 2:
				System.out.println();
				System.out.print("Enter book ID to search :");
				int bookId=in.nextInt();
				try {
					System.out.println(new BookData().searchBook(bookId));
					System.out.println("Book search found!");
				}catch (RecordEmptyException e) {
					System.out.println(e.getMessage());
				}catch(NoSuchElementException e){
					System.out.println(e.getMessage());
				}
				System.out.println();
				break;
			case 3:
				Book book= new Book(9,"hero","arun",9000.0,Date.valueOf("2021-12-7"));
				if(new BookData().addBook(book))
					System.out.println("Book added!");
				else
					System.out.println("Book not added");
				System.out.println();
				break;
			case 4:
				System.out.println();
				System.out.print("Enter book ID to delete the book :");
				int bookIdfor=in.nextInt();
				try {
					if(new BookData().deleteBook(bookIdfor)) {
						System.out.println("Book deleted!");
					}
				}catch(NoSuchElementException e) {
					System.out.println(e.getMessage());
				}
				System.out.println();
				break;
			case 5:
				System.out.println();
				System.out.println("Enter book ID for any updates");
				try {
					new BookData().updateBook(2,"hero","arun",9000,Date.valueOf("8888-12-7"));
				} catch (NoSuchElementException |RecordEmptyException e) {
					e.getMessage();
				}
				System.out.println("Book Updated!");
				System.out.println();
				break;
			case 0:
				System.exit(0);
			}
		}while(true);

	}

}


