package database;

import java.sql.Date;
import java.util.NoSuchElementException;
import java.util.TreeSet;

import exceptions.RecordEmptyException;
import model.Book;

public class BookData{
	private static TreeSet<Book> books = new TreeSet<Book>();

	public TreeSet<Book> viewBook() throws RecordEmptyException{
		if(!books.isEmpty()) {
			return books;
		}
		else {
			throw new RecordEmptyException("No records of book!");
		}
	}

	public Book searchBook(int bookId) throws RecordEmptyException{

		if(!books.isEmpty()) {
			for(Book book:books) {
				if(book.getBookid()==bookId) {
					return book;
				}
			}
			throw new NoSuchElementException("Book "+bookId+" not foud in book records!");
		}
		throw new RecordEmptyException("Records are empty!");
	}

	public boolean addBook(Book book) {
		if(books.add(book)) {
			//System.out.println(books);
			return true;
		}
		return false;

	}

	public boolean deleteBook(int bookId) {
		Book book=null;
		try {
			book=searchBook(bookId);
			books.remove(book);
		}
		catch(RecordEmptyException e){
			e.printStackTrace();
		}
		return true;
	}

	public void updateBook(int bid,String utitle,String ustring,double uprice,Date date) throws RecordEmptyException{
		try {
			Book b1=searchBook(bid);
			b1.setBookid(bid);
			b1.setTitle(utitle);
			b1.setAuthor(ustring);
			b1.setPrice(uprice);
			b1.setPublishDt(date);
		}
		catch(RecordEmptyException e){
			e.printStackTrace();
		}
	}
}